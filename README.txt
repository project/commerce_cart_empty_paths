ABOUT THIS MODULE
-----------------

This module allows site administrators to define multiple paths, and when a
user visits a non-declared page, their shopping cart is emptied.

Current maintainer: Oliver Davies (https://drupal.org/u/opdavies)

USAGE
-----

* Download and enable the module.
* Go to admin/commerce/config/empty-cart-paths and enter any additional paths.
  'checkout' and 'checkout/*' are declared by default.
* That's it!

